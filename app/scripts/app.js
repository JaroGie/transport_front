'use strict';

/**
 * @ngdoc overview
 * @name transpFrontApp
 * @description
 * # transpFrontApp
 *
 * Main module of the application.
 */
angular
  .module('transpFrontApp', [
    'ngCookies',
    'ngResource',
    'ngRoute',
    'ngSanitize',
    'ui.bootstrap'
  ])
  .config(function ($routeProvider) {
    $routeProvider
      .when('/', {
        templateUrl: 'views/main.html',
        controller: 'MainCtrl'
      })
      .when('/about', {
        templateUrl: 'views/about.html',
        controller: 'AboutCtrl'
      })
      .when('/clients', {
        templateUrl: 'views/clientslist.html',
        controller: 'ClientslistCtrl'
      })
      .when('/client/:id', {
        templateUrl: 'views/clientdetail.html',
        controller: 'ClientdetailCtrl'
      })
      .when('/client/:id/orders', {
        templateUrl: 'views/orderslist.html',
        controller: 'OrderslistCtrl'
      })
      .when('/client/:id/:orderId', {
        templateUrl: 'views/orderdetail.html',
        controller: 'OrderdetailCtrl'
      })
      .when('/drivers', {
        templateUrl: 'views/driverslist.html',
        controller: 'DriverslistCtrl'
      })
      .when('/driver/:id', {
        templateUrl: 'views/driverdetail.html',
        controller: 'DriverdetailCtrl'
      })
      .when('/trailers', {
        templateUrl: 'views/trailerslist.html',
        controller: 'TrailerslistCtrl'
      })
      .when('/trailer/:id', {
        templateUrl: 'views/trailerdetail.html',
        controller: 'TrailerdetailCtrl'
      })
      .when('/trailer/:id/services', {
        templateUrl: 'views/trailerserviceslist.html',
        controller: 'TrailerserviceslistCtrl'
      })
      .when('/trailer/:id/:serviceId', {
        templateUrl: 'views/trailerservicedetail.html',
        controller: 'TrailerservicedetailCtrl'
      })
      .when('/trucks', {
        templateUrl: 'views/truckslist.html',
        controller: 'TruckslistCtrl'
      })
      .when('/truck/:id', {
        templateUrl: 'views/truckdetail.html',
        controller: 'TruckdetailCtrl'
      })
      .when('/truck/:id/services', {
        templateUrl: 'views/truckserviceslist.html',
        controller: 'TruckserviceslistCtrl'
      })
      .when('/truck/:id/:serviceId', {
        templateUrl: 'views/truckservicedetail.html',
        controller: 'TruckservicedetailCtrl'
      })
      .when('/client/:id/:orderId/loadPlace/:loadplaceId', {
        templateUrl: 'views/loadplacedetail.html',
        controller: 'LoadplacedetailCtrl'
      })
      .when('/client/:id/:orderId/unloadPlace/:unloadplaceId', {
        templateUrl: 'views/unloadplacedetail.html',
        controller: 'UnloadplacedetailCtrl'
      })
      .when('/reports', {
        templateUrl: 'views/report.html',
        controller: 'ReportCtrl'
      })
      .otherwise({
        redirectTo: '/'
      });
  });
