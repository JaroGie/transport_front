'use strict';

/**
 * @ngdoc function
 * @name transpFrontApp.controller:ReportCtrl
 * @description
 * # ReportCtrl
 * Controller of the transpFrontApp
 */
angular.module('transpFrontApp')
  .controller('ReportCtrl', function ($http, $scope, $filter) {

    var startDate = new Date(Date.now());
    startDate.setFullYear(startDate.getFullYear() - 15);
    var endDate = new Date(Date.now());

    //$scope.filter = {
    //  startDateParamDriver: $filter('date')(startDate, 'yyyy-MM-dd'),
    //  endDateParamDriver: $filter('date')(endDate, 'yyyy-MM-dd'),
    //
    //  startDateParamTruck: $filter('date')(startDate, 'yyyy-MM-dd'),
    //  endDateParamTruck: $filter('date')(endDate, 'yyyy-MM-dd'),
    //
    //  startDateParamTrailer: $filter('date')(startDate, 'yyyy-MM-dd'),
    //  endDateParamTrailer: $filter('date')(endDate, 'yyyy-MM-dd')
    //};


    $scope.startDateParamDriver = $filter('date')(startDate, 'yyyy-MM-dd');
    $scope.endDateParamDriver = $filter('date')(endDate, 'yyyy-MM-dd');

    $scope.startDateParamTruck = $filter('date')(startDate, 'yyyy-MM-dd');
    $scope.endDateParamTruck = $filter('date')(endDate, 'yyyy-MM-dd');

    $scope.startDateParamTrailer = $filter('date')(startDate, 'yyyy-MM-dd');
    $scope.endDateParamTrailer = $filter('date')(endDate, 'yyyy-MM-dd');


  });
