'use strict';

/**
 * @ngdoc function
 * @name transpFrontApp.controller:TrailerdetailCtrl
 * @description
 * # TrailerdetailCtrl
 * Controller of the transpFrontApp
 */
angular.module('transpFrontApp')
  .controller('TrailerdetailCtrl', function ($scope, $filter, $location, $routeParams, TrailerDAO, TruckDAO, TrailerWorkshopDAO) {

    var startDate = new Date(Date.now());
    startDate.setFullYear(startDate.getFullYear() - 10);
    var endDate = new Date(Date.now());

    $scope.filter = {
      first: 0,
      last: 5,
      startDateParam: $filter('date')(startDate, 'yyyy-MM-dd'),
      endDateParam: $filter('date')(endDate, 'yyyy-MM-dd'),
      sortParam: 'trailerInspectionDate',
      sortType: 'DESC'
    };

    $scope.pagination = {currentPage: 1, itemsPerPage: 5};
    $scope.filter.startDateParam = new Date($filter('date')(startDate, 'yyyy-MM-dd'));
    $scope.filter.endDateParam = new Date($filter('date')(endDate, 'yyyy-MM-dd'));

    $scope.filter2 = {
      searchParam: '',
      first: 0,
      last: 5,
      startDateParam: $filter('date')(startDate, 'yyyy-MM-dd'),
      endDateParam: $filter('date')(endDate, 'yyyy-MM-dd'),
      sortParam: 'orderDate',
      sortType: 'DESC'
    };

    $scope.pagination2 = {currentPage: 1, itemsPerPage: 5};
    $scope.filter2.startDateParam = new Date($filter('date')(startDate, 'yyyy-MM-dd'));
    $scope.filter2.endDateParam = new Date($filter('date')(endDate, 'yyyy-MM-dd'));

    $scope.trailer = {};
    $scope.client = {};
    $scope.showTabs = $routeParams.id !== 'new';

    var refreshTrucks = function () {
      if ($routeParams.id !== 'new') {
        TrailerDAO.getTrucks($routeParams.id).then(function (data) {
          $scope.trucksList = data;
          $scope.trucksCount = data.length;
        });
      }
    };

    var refreshTrailerWorkshops = function () {
      if ($routeParams.id !== 'new') {
        TrailerDAO.getTrailerWorkshops($routeParams.id, $scope.filter).then(function (data) {
          $scope.trailerWorkshopsList = data.result;
          $scope.pagination.totalItems = data.total;
          $scope.servicesCount = data.total;
        });
      }
    };

    var refreshOrders = function () {
      if ($routeParams.id !== 'new') {
        TrailerDAO.getOrders($routeParams.id, $scope.filter2).then(function (data) {
          $scope.ordersList = data.result;
          $scope.pagination2.totalItems = data.total;
          $scope.ordersCount = data.total;
        });
      }
    };

    var refreshDetail = function () {
      if ($routeParams.id !== 'new') {
        TrailerDAO.get($routeParams.id).then(function (data) {
          $scope.trailer = data;
          $scope.trailer.id = data.id;
          $scope.trailer.manufacturingYear = new Date(data.manufacturingYear);
        });
      }
    };

    $scope.saveTrailer = function () {
      if ($routeParams.id === 'new') {
        TrailerDAO.save($scope.trailer).then(function () {
          $location.path('/trailers');
        });
      } else {
        TrailerDAO.update($routeParams.id, $scope.trailer).then(function () {
          $location.path('/trailers');
        });
      }
    };

    $scope.getTrucks = function () {
      var filter = {
        searchParam: '',
        first: 0,
        last: 5,
        startDateParam: '1970-01-01',
        endDateParam: '2100-01-01',
        sortParam: 'brand',
        sortType: 'ASC'
      };

      TruckDAO.query(filter).then(function (data) {
        $scope.trucks = data.result;
        angular.forEach($scope.trucks, function (value) {
          value['text'] = value.brand + ' ' + value.model + ' ' + value.registrationNumber;
        })
      });
    };

    $scope.selectTruck = function ($item, $model, $label) {
      $scope.truckId = '' + $item.id;
    };

    $scope.addTruck = function () {
      TrailerDAO.addTruck($routeParams.id, $scope.truckId).then(function () {
        $scope.id = false;
        refreshTrucks();
      });
    };

    $scope.removeTruck = function (id) {
      TrailerDAO.removeTruck($routeParams.id, id).then(refreshTrucks);
    };

    $scope.removeTrailerWorkshop = function (id) {
      TrailerWorkshopDAO.remove(id).then(refreshTrailerWorkshops);
    };

    $scope.pageChange = function () {
      $scope.filter.first = ($scope.pagination.currentPage * $scope.pagination.itemsPerPage) - $scope.pagination.itemsPerPage;
      refreshTrailerWorkshops();
    };

    $scope.pageChange2 = function () {
      $scope.filter2.first = ($scope.pagination2.currentPage * $scope.pagination2.itemsPerPage) - $scope.pagination2.itemsPerPage;
      refreshOrders();
    };

    $scope.search = function () {
      $scope.pagination.itemsPerPage = $scope.filter.last;
      refreshTrailerWorkshops();
    };

    $scope.search2 = function () {
      $scope.pagination2.itemsPerPage = $scope.filter2.last;
      refreshOrders();
    };

    $scope.sortColumn = function (sortParam) {
      if ($scope.filter.sortType === 'ASC') {
        $scope.filter.sortType = 'DESC';
      } else {
        $scope.filter.sortType = 'ASC';
      }
      $scope.filter.sortParam = sortParam;
      refreshList();
    };

    $scope.sortColumn2 = function (sortParam) {
      if ($scope.filter2.sortType === 'ASC') {
        $scope.filter2.sortType = 'DESC';
      } else {
        $scope.filter2.sortType = 'ASC';
      }
      $scope.filter2.sortParam = sortParam;
      refreshOrders();
    };

    refreshDetail();
    refreshTrucks();
    refreshTrailerWorkshops();
    refreshOrders();
  });
