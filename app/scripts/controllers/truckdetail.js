'use strict';

/**
 * @ngdoc function
 * @name transpFrontApp.controller:TruckdetailCtrl
 * @description
 * # TruckdetailCtrl
 * Controller of the transpFrontApp
 */
angular.module('transpFrontApp')
  .controller('TruckdetailCtrl', function ($scope, $filter, $location, $routeParams, TruckDAO, DriverDAO, TruckWorkshopDAO) {

    var startDate = new Date(Date.now());
    startDate.setFullYear(startDate.getFullYear() - 10);
    var endDate = new Date(Date.now());

    $scope.filter = {
      first: 0,
      last: 5,
      startDateParam: $filter('date')(startDate, 'yyyy-MM-dd'),
      endDateParam: $filter('date')(endDate, 'yyyy-MM-dd'),
      sortParam: 'truckInspectionDate',
      sortType: 'DESC'
    };

    $scope.pagination = {currentPage: 1, itemsPerPage: 5};
    $scope.filter.startDateParam = new Date($filter('date')(startDate, 'yyyy-MM-dd'));
    $scope.filter.endDateParam = new Date($filter('date')(endDate, 'yyyy-MM-dd'));

    $scope.truck = {};
    $scope.showTabs = $routeParams.id !== 'new';

    var refreshDetail = function () {
      if ($routeParams.id !== 'new') {
        TruckDAO.get($routeParams.id).then(function (data) {
          $scope.truck = data;
          $scope.truck.id = data.id;
          $scope.truck.manufacturingYear = new Date(data.manufacturingYear);
          $scope.truck.set = data.trailer.brand + ' ' + data.trailer.model + ' - ' + data.trailer.registrationNumber;
        });
      }
    };

    var refreshDrivers = function () {
      if ($routeParams.id !== 'new') {
        TruckDAO.getDrivers($routeParams.id).then(function (data) {
          $scope.driversList = data;
          $scope.driversCount = data.length;
        });
      }
    };

    var refreshTruckWorkshops = function () {
      if ($routeParams.id !== 'new') {
        TruckDAO.getTruckWorkshops($routeParams.id, $scope.filter).then(function (data) {
          $scope.truckWorkshopsList = data.result;
          $scope.pagination.totalItems = data.total;
          $scope.servicesCount = data.total;
        });
      }
    };

    $scope.saveTruck = function () {
      if ($routeParams.id === 'new') {
        TruckDAO.save($scope.truck).then(function () {
          $location.path('/trucks');
        });
      } else {
        TruckDAO.update($routeParams.id, $scope.truck).then(function () {
          $location.path('/trucks');
        });
      }
    };

    $scope.getDrivers = function () {
      var filter = {
        searchParam: '',
        first: 0,
        last: 0,
        startDateParam: '1970-01-01',
        endDateParam: '2100-01-01',
        sortParam: 'surname',
        sortType: 'ASC'
      };

      DriverDAO.query(filter).then(function (data) {
        $scope.drivers = data.result;
        angular.forEach($scope.drivers, function (value) {
          if (value.adrCertificate !== null) {
            value['text'] = value.name + ' ' + value.surname + ' ' + value.driverCardId + ' ( ' + value.adrCertificate + ' )';
          } else {
            value['text'] = value.name + ' ' + value.surname + ' ' + value.driverCardId;
          }
        })
      });
    };

    $scope.selectDriver = function ($item, $model, $label) {
      $scope.driverId = '' + $item.id;
    };

    $scope.addDriver = function () {
      TruckDAO.addDriver($routeParams.id, $scope.driverId).then(function () {
        $scope.id = false;
        refreshDrivers();
      });
    };

    $scope.removeDriver = function (id) {
      TruckDAO.removeDriver($routeParams.id, id).then(refreshDrivers);
    };

    $scope.removeTruckWorkshop = function (id) {
      TruckWorkshopDAO.remove(id).then(refreshTruckWorkshops);
    };

    $scope.pageChange = function () {
      $scope.filter.first = ($scope.pagination.currentPage * $scope.pagination.itemsPerPage) - $scope.pagination.itemsPerPage;
      refreshTruckWorkshops();
    };

    $scope.search = function () {
      $scope.pagination.itemsPerPage = $scope.filter.last;
      refreshTruckWorkshops();
    };

    $scope.sortColumn = function (sortParam) {
      if ($scope.filter.sortType === 'ASC') {
        $scope.filter.sortType = 'DESC';
      } else {
        $scope.filter.sortType = 'ASC';
      }
      $scope.filter.sortParam = sortParam;
      refreshList();
    };

    refreshDetail();
    refreshDrivers();
    refreshTruckWorkshops();
  });
