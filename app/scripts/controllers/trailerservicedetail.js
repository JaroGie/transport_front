'use strict';

/**
 * @ngdoc function
 * @name transpFrontApp.controller:TrailerservicedetailCtrl
 * @description
 * # TrailerservicedetailCtrl
 * Controller of the transpFrontApp
 */
angular.module('transpFrontApp')
  .controller('TrailerservicedetailCtrl', function ($scope, $location, $routeParams, TrailerWorkshopDAO, TrailerDAO) {

    $scope.trailer = {};

    var refreshTrailer = function () {
      TrailerDAO.get($routeParams.id).then(function (data) {
        $scope.trailer.name = data.brand + ' ' + data.model + ' - ' + data.registrationNumber;
        $scope.trailer.id = data.id;
      });
    };

    var refreshDetail = function () {
      if ($routeParams.serviceId !== 'new') {
        TrailerWorkshopDAO.get($routeParams.serviceId).then(function (data) {
          $scope.trailerWorkshop = data;
          $scope.trailerWorkshop.trailerInspectionDate = new Date(data.trailerInspectionDate);
          $scope.trailerWorkshop.trailerInspectionExpiryDate = new Date(data.trailerInspectionExpiryDate);
        });
      }
    };

    $scope.saveTrailerWorkshop = function () {
      if ($routeParams.serviceId === 'new') {
        TrailerDAO.addTrailerWorkshop($routeParams.id, $scope.trailerWorkshop).then(function () {
          $location.path('/trailer/' + $routeParams.id);
        });
      } else {
        TrailerWorkshopDAO.update($routeParams.serviceId, $scope.trailerWorkshop).then(function () {
          $location.path('/trailer/' + $routeParams.id);
        });
      }
    };

    refreshTrailer();
    refreshDetail();
  });
