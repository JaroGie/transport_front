'use strict';

/**
 * @ngdoc function
 * @name transpFrontApp.controller:DriverdetailCtrl
 * @description
 * # DriverdetailCtrl
 * Controller of the transpFrontApp
 */
angular.module('transpFrontApp')
  .controller('DriverdetailCtrl', function ($scope, $location, $routeParams, DriverDAO) {

    var refreshDetail = function () {
      if ($routeParams.id !== 'new') {
        DriverDAO.get($routeParams.id).then(function (data) {
          $scope.driver = data;
          $scope.driver.employmentDate = new Date(data.employmentDate);
          $scope.driver.medicalExaminationExpiryDate = new Date(data.medicalExaminationExpiryDate);
          $scope.driver.set = data.truck.brand + ' ' + data.truck.model + ' ' + data.truck.engineModel + ' - ' + data.truck.registrationNumber
            + '   +   ' + data.truck.trailer.brand + ' ' + data.truck.trailer.model + ' - ' + data.truck.trailer.registrationNumber;
        });
      }
    };

    $scope.saveDriver = function () {
      if ($routeParams.id === 'new') {
        DriverDAO.save($scope.driver).then(function () {
          $location.path('/drivers');
        });
      } else {
        DriverDAO.update($routeParams.id, $scope.driver).then(function () {
          $location.path('/drivers');
        });
      }
    };

    refreshDetail();
  });
