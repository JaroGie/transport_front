'use strict';

/**
 * @ngdoc function
 * @name transpFrontApp.controller:OrderdetailCtrl
 * @description
 * # OrderdetailCtrl
 * Controller of the transpFrontApp
 */
angular.module('transpFrontApp')
  .controller('OrderdetailCtrl', function ($scope, $routeParams, $location, ClientDAO, OrderDAO, TrailerDAO, LoadPlaceDAO, UnloadPlaceDAO) {

    $scope.client = {};
    $scope.order = {deliveryStatus: false, receiveMoney: false};
    $scope.showTabs = $routeParams.orderId !== 'new';

    var refreshClient = function () {
      ClientDAO.get($routeParams.id).then(function (data) {
        $scope.client.name = data.name + ' ' + data.surname + ' - ' + data.companyName;
        $scope.client.id = data.id;
      });
    };

    var refreshDetail = function () {
      if ($routeParams.orderId !== 'new') {
        OrderDAO.get($routeParams.orderId).then(function (data) {
          $scope.order = data;
          $scope.order.orderDate = new Date(data.orderDate);
        });
      }
    };

    var refreshTrailers = function () {
      if ($routeParams.orderId !== 'new') {
        OrderDAO.getTrailers($routeParams.orderId).then(function (data) {
          $scope.trailersList = data;
          $scope.trailersCount = data.length;
        });
      }
    };

    var refreshLoadPlaces = function () {
      if ($routeParams.orderId !== 'new') {
        OrderDAO.getLoadPlaces($routeParams.orderId).then(function (data) {
          $scope.loadPlacesList = data;
          $scope.loadPlacesCount = data.length;
        });
      }
    };

    var refreshUnloadPlaces = function () {
      if ($routeParams.orderId !== 'new') {
        OrderDAO.getUnloadPlaces($routeParams.orderId).then(function (data) {
          $scope.unloadPlacesList = data;
          $scope.unloadPlacesCount = data.length;
        });
      }
    };

    $scope.saveOrder = function () {
      if ($routeParams.orderId === 'new') {
        ClientDAO.addOrder($routeParams.id, $scope.order).then(function () {
          $location.path('/client/' + $routeParams.id + '/orders');
        });
      } else {
        OrderDAO.update($routeParams.orderId, $scope.order).then(function () {
          $location.path('/client/' + $routeParams.id + '/orders');
        });
      }
    };

    $scope.getTrailers = function () {
      var filter = {
        searchParam: '',
        first: 0,
        last: 0,
        startDateParam: '1970-01-01',
        endDateParam: '2100-01-01',
        sortParam: 'brand',
        sortType: 'ASC'
      };

      TrailerDAO.query(filter).then(function (data) {
        $scope.trailers = data.result;
        angular.forEach($scope.trailers, function (value) {
          if (value.adrCertificate !== null) {
            value['text'] = value.brand + ' ' + value.model + ' ' + value.registrationNumber + ' ( ' + value.adrCertificate + ' )';
          } else {
            value['text'] = value.brand + ' ' + value.model + ' ' + value.registrationNumber;
          }
        })
      });
    };

    $scope.selectTrailer = function ($item, $model, $label) {
      $scope.trailerId = '' + $item.id;
    };

    $scope.addTrailer = function () {
      OrderDAO.addTrailer($routeParams.orderId, $scope.trailerId).then(function () {
        //$scope.trailerId = false;
        refreshTrailers();
      });
    };

    $scope.removeTrailer = function (id) {
      OrderDAO.removeTrailer($routeParams.orderId, id).then(refreshTrailers);
    };

    $scope.removeLoadPlace = function (id) {
      LoadPlaceDAO.remove(id).then(refreshLoadPlaces);
    };

    $scope.removeUnloadPlace = function (id) {
      UnloadPlaceDAO.remove(id).then(refreshUnloadPlaces);
    };

    refreshClient();
    refreshDetail();
    refreshTrailers();
    refreshLoadPlaces();
    refreshUnloadPlaces();
  });
