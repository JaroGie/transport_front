'use strict';

/**
 * @ngdoc function
 * @name transpFrontApp.controller:TruckserviceslistCtrl
 * @description
 * # TruckserviceslistCtrl
 * Controller of the transpFrontApp
 */
angular.module('transpFrontApp')
  .controller('TruckserviceslistCtrl', function ($scope) {
    $scope.awesomeThings = [
      'HTML5 Boilerplate',
      'AngularJS',
      'Karma'
    ];
  });
