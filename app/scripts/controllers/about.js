'use strict';

/**
 * @ngdoc function
 * @name transpFrontApp.controller:AboutCtrl
 * @description
 * # AboutCtrl
 * Controller of the transpFrontApp
 */
angular.module('transpFrontApp')
  .controller('AboutCtrl', function ($scope) {
    $scope.awesomeThings = [
      'HTML5 Boilerplate',
      'AngularJS',
      'Karma'
    ];
  });
