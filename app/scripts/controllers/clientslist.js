'use strict';

/**
 * @ngdoc function
 * @name transpFrontApp.controller:ClientslistCtrl
 * @description
 * # ClientslistCtrl
 * Controller of the transpFrontApp
 */
angular.module('transpFrontApp')
  .controller('ClientslistCtrl', function ($scope, ClientDAO) {

    $scope.filter = {searchParam: '', first: 0, last: 5, sortParam: 'surname', sortType: 'ASC'};
    $scope.pagination = {currentPage: 1, itemsPerPage: 5};

    var refreshList = function () {
      ClientDAO.query($scope.filter).then(function (data) {
        $scope.list = data.result;
        $scope.pagination.totalItems = data.total;
      });
    };

    $scope.pageChange = function () {
      $scope.filter.first = ($scope.pagination.currentPage * $scope.pagination.itemsPerPage) - $scope.pagination.itemsPerPage;
      refreshList();
    };

    $scope.removeClient = function (id) {
      ClientDAO.remove(id).then(refreshList);
    };

    $scope.search = function () {
      $scope.pagination.itemsPerPage = $scope.filter.last;
      refreshList();
    };

    $scope.sortColumn = function (sortParam) {
      if ($scope.filter.sortType === 'ASC') {
        $scope.filter.sortType = 'DESC';
      } else {
        $scope.filter.sortType = 'ASC';
      }
      $scope.filter.sortParam = sortParam;
      refreshList();
    };

    refreshList();
  });
