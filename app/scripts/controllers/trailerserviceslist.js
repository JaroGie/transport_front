'use strict';

/**
 * @ngdoc function
 * @name transpFrontApp.controller:TrailerserviceslistCtrl
 * @description
 * # TrailerserviceslistCtrl
 * Controller of the transpFrontApp
 */
angular.module('transpFrontApp')
  .controller('TrailerserviceslistCtrl', function ($scope) {
    $scope.awesomeThings = [
      'HTML5 Boilerplate',
      'AngularJS',
      'Karma'
    ];
  });
