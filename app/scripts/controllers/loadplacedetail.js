'use strict';

/**
 * @ngdoc function
 * @name transpFrontApp.controller:LoadplacedetailCtrl
 * @description
 * # LoadplacedetailCtrl
 * Controller of the transpFrontApp
 */
angular.module('transpFrontApp')
  .controller('LoadplacedetailCtrl', function ($scope, $routeParams, $location, ClientDAO, OrderDAO, LoadPlaceDAO) {

    $scope.client = {};
    $scope.order = {};
    var refreshClient = function () {
      ClientDAO.get($routeParams.id).then(function (data) {
        $scope.client.id = data.id;
      });
    };

    var refreshOrder = function () {
      if ($routeParams.orderId !== 'new') {
        OrderDAO.get($routeParams.orderId).then(function (data) {
          $scope.order.id = data.id;
          $scope.order.name = data.client.name + ' ' + data.client.surname + ' - ' + data.client.companyName + ' - ' + data.cargoDescription;
        });
      }
    };

    var refreshDetail = function () {
      if ($routeParams.loadplaceId !== 'new') {
        LoadPlaceDAO.get($routeParams.loadplaceId).then(function (data) {
          $scope.loadPlace = data;
          $scope.loadPlace.date = new Date(data.date);
        });
      }
    };

    $scope.saveLoadPlace = function () {
      if ($routeParams.loadplaceId === 'new') {
        OrderDAO.addLoadPlace($routeParams.orderId, $scope.loadPlace).then(function () {
          $location.path('/client' + '/' + $routeParams.id + '/' + $routeParams.orderId);
        });
      } else {
        LoadPlaceDAO.update($routeParams.loadplaceId, $scope.loadPlace).then(function () {
          $location.path('/client' + '/' + $routeParams.id + '/' + $routeParams.orderId);
        });
      }
    };

    refreshClient();
    refreshOrder();
    refreshDetail();
  });
