'use strict';

/**
 * @ngdoc function
 * @name transpFrontApp.controller:TruckservicedetailCtrl
 * @description
 * # TruckservicedetailCtrl
 * Controller of the transpFrontApp
 */
angular.module('transpFrontApp')
  .controller('TruckservicedetailCtrl', function ($scope, $location, $routeParams, TruckWorkshopDAO, TruckDAO) {

    $scope.truck = {};

    var refreshTruck = function () {
      TruckDAO.get($routeParams.id).then(function (data) {
        $scope.truck.name = data.brand + ' ' + data.model + ' - ' + data.registrationNumber;
        $scope.truck.id = data.id;
      });
    };

    var refreshDetail = function () {
      if ($routeParams.serviceId !== 'new') {
        TruckWorkshopDAO.get($routeParams.serviceId).then(function (data) {
          $scope.truckWorkshop = data;
          $scope.truckWorkshop.truckInspectionDate = new Date(data.truckInspectionDate);
          $scope.truckWorkshop.truckInspectionExpiryDate = new Date(data.truckInspectionExpiryDate);
        });
      }
    };

    $scope.saveTruckWorkshop = function () {
      if ($routeParams.serviceId === 'new') {
        TruckDAO.addTruckWorkshop($routeParams.id, $scope.truckWorkshop).then(function () {
          $location.path('/truck/' + $routeParams.id);
        });
      } else {
        TruckWorkshopDAO.update($routeParams.serviceId, $scope.truckWorkshop).then(function () {
          $location.path('/truck/' + $routeParams.id);
        });
      }
    };

    refreshTruck();
    refreshDetail();
  });
