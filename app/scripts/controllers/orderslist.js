'use strict';

/**
 * @ngdoc function
 * @name transpFrontApp.controller:OrderslistCtrl
 * @description
 * # OrderslistCtrl
 * Controller of the transpFrontApp
 */
angular.module('transpFrontApp')
  .controller('OrderslistCtrl', function ($scope, $filter, $routeParams, ClientDAO, OrderDAO) {

    var startDate = new Date(Date.now());
    startDate.setFullYear(startDate.getFullYear() - 10);
    var endDate = new Date(Date.now());
    endDate.setMonth(endDate.getMonth() + 3);

    $scope.filter = {
      searchParam: '',
      first: 0,
      last: 5,
      startDateParam: $filter('date')(startDate, 'yyyy-MM-dd'),
      endDateParam: $filter('date')(endDate, 'yyyy-MM-dd'),
      sortParam: 'orderDate',
      sortType: 'DESC'
    };

    $scope.pagination = {currentPage: 1, itemsPerPage: 5};
    $scope.filter.startDateParam = new Date($filter('date')(startDate, 'yyyy-MM-dd'));
    $scope.filter.endDateParam = new Date($filter('date')(endDate, 'yyyy-MM-dd'));
    $scope.client = {};

    ClientDAO.get($routeParams.id).then(function (data) {
      $scope.client.name = data.name + ' ' + data.surname + ' ' + data.companyName;
      $scope.client.id = data.id;
    });

    var refreshList = function () {
      ClientDAO.getOrders($routeParams.id, $scope.filter).then(function (data) {
        $scope.list = data.result;
        $scope.pagination.totalItems = data.total;
      });
    };

    $scope.removeOrder = function (id) {
      OrderDAO.remove(id).then(refreshList);
    };

    $scope.pageChange = function () {
      $scope.filter.first = ($scope.pagination.currentPage * $scope.pagination.itemsPerPage) - $scope.pagination.itemsPerPage;
      refreshList();
    };

    $scope.search = function () {
      $scope.pagination.itemsPerPage = $scope.filter.last;
      refreshList();
    };

    $scope.sortColumn = function (sortParam) {
      if ($scope.filter.sortType === 'ASC') {
        $scope.filter.sortType = 'DESC';
      } else {
        $scope.filter.sortType = 'ASC';
      }
      $scope.filter.sortParam = sortParam;
      refreshList();
    };

    refreshList();
  });
