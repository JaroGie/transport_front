'use strict';

/**
 * @ngdoc function
 * @name transpFrontApp.controller:UnloadplacedetailCtrl
 * @description
 * # UnloadplacedetailCtrl
 * Controller of the transpFrontApp
 */
angular.module('transpFrontApp')
  .controller('UnloadplacedetailCtrl', function ($scope, $routeParams, $location, ClientDAO, OrderDAO, UnloadPlaceDAO) {

    $scope.client = {};
    $scope.order = {};
    var refreshClient = function () {
      ClientDAO.get($routeParams.id).then(function (data) {
        $scope.client.id = data.id;
      });
    };

    var refreshOrder = function () {
      if ($routeParams.orderId !== 'new') {
        OrderDAO.get($routeParams.orderId).then(function (data) {
          $scope.order.id = data.id;
          $scope.order.name = data.client.name + ' ' + data.client.surname + ' - ' + data.client.companyName + ' - ' + data.cargoDescription;
        });
      }
    };

    var refreshDetail = function () {
      if ($routeParams.unloadplaceId !== 'new') {
        UnloadPlaceDAO.get($routeParams.unloadplaceId).then(function (data) {
          $scope.unloadPlace = data;
          $scope.unloadPlace.date = new Date(data.date);
        });
      }
    };

    $scope.saveUnloadPlace = function () {
      if ($routeParams.unloadplaceId === 'new') {
        OrderDAO.addUnloadPlace($routeParams.orderId, $scope.unloadPlace).then(function () {
          $location.path('/client' + '/' + $routeParams.id + '/' + $routeParams.orderId);
        });
      } else {
        UnloadPlaceDAO.update($routeParams.unloadplaceId, $scope.unloadPlace).then(function () {
          $location.path('/client' + '/' + $routeParams.id + '/' + $routeParams.orderId);
        });
      }
    };

    refreshClient();
    refreshOrder();
    refreshDetail();
  });
