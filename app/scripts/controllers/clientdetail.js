'use strict';

/**
 * @ngdoc function
 * @name transpFrontApp.controller:ClientdetailCtrl
 * @description
 * # ClientdetailCtrl
 * Controller of the transpFrontApp
 */
angular.module('transpFrontApp')
  .controller('ClientdetailCtrl', function ($scope, $location, $routeParams, ClientDAO) {

    var refreshDetail = function () {
      if ($routeParams.id !== 'new') {
        ClientDAO.get($routeParams.id).then(function (data) {
          $scope.client = data;
        });
      }
    };

    $scope.saveClient = function () {
      if ($routeParams.id === 'new') {
        ClientDAO.save($scope.client).then(function () {
          $location.path('/clients');
        });
      } else {
        ClientDAO.update($routeParams.id, $scope.client).then(function () {
          $location.path('/clients');
        });
      }
    };

    refreshDetail();
  });
