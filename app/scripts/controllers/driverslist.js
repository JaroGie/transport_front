'use strict';

/**
 * @ngdoc function
 * @name transpFrontApp.controller:DriverslistCtrl
 * @description
 * # DriverslistCtrl
 * Controller of the transpFrontApp
 */
angular.module('transpFrontApp')
  .controller('DriverslistCtrl', function ($scope, $filter, DriverDAO) {

    var startDate = new Date(Date.now());
    startDate.setFullYear(startDate.getFullYear() - 25);
    var endDate = new Date(Date.now());

    $scope.filter = {
      searchParam: '',
      first: 0,
      last: 5,
      startDateParam: $filter('date')(startDate, 'yyyy-MM-dd'),
      endDateParam: $filter('date')(endDate, 'yyyy-MM-dd'),
      sortParam: 'surname',
      sortType: 'ASC'
    };
    $scope.pagination = {currentPage: 1, itemsPerPage: 5};
    $scope.filter.startDateParam = new Date($filter('date')(startDate, 'yyyy-MM-dd'));
    $scope.filter.endDateParam = new Date(Date.now());

    var refreshList = function () {
      DriverDAO.query($scope.filter).then(function (data) {
        $scope.list = data.result;
        $scope.pagination.totalItems = data.total;
      });
    };

    $scope.pageChange = function () {
      $scope.filter.first = ($scope.pagination.currentPage * $scope.pagination.itemsPerPage) - $scope.pagination.itemsPerPage;
      refreshList();
    };

    $scope.removeDriver = function (id) {
      DriverDAO.remove(id).then(refreshList);
    };

    $scope.search = function () {
      $scope.pagination.itemsPerPage = $scope.filter.last;
      refreshList();
    };

    $scope.sortColumn = function (sortParam) {
      if ($scope.filter.sortType === 'ASC') {
        $scope.filter.sortType = 'DESC';
      } else {
        $scope.filter.sortType = 'ASC';
      }
      $scope.filter.sortParam = sortParam;
      refreshList();
    };

    refreshList();
  });
