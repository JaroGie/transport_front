'use strict';

/**
 * @ngdoc function
 * @name transpFrontApp.controller:MainCtrl
 * @description
 * # MainCtrl
 * Controller of the transpFrontApp
 */
angular.module('transpFrontApp')
  .controller('MainCtrl', function ($scope, $filter, ClientDAO, DriverDAO, TruckDAO, TrailerDAO, OrderDAO, TruckWorkshopDAO, TrailerWorkshopDAO) {

    $scope.filter = {searchParam: '', first: 0, last: 5, sortParam: 'surname', sortType: 'ASC'};
      ClientDAO.query($scope.filter).then(function (data) {
        $scope.clients = data.total;
      });

    $scope.filter2 = {
      searchParam: '',
      first: 0,
      last: 0,
      startDateParam: '1970-01-01',
      endDateParam: '2100-01-01',
      sortParam: 'surname',
      sortType: 'ASC'
    };
    DriverDAO.query($scope.filter2).then(function (data) {
        $scope.drivers = data.total;
      });

    $scope.filter3 = {
      searchParam: '',
      first: 0,
      last: 0,
      startDateParam: '1970-01-01',
      endDateParam: '2100-01-01',
      sortParam: 'brand',
      sortType: 'ASC'
    };
    TruckDAO.query($scope.filter3).then(function (data) {
        $scope.trucks = data.total;
      });

    $scope.filter4 = {
      searchParam: '',
      first: 0,
      last: 0,
      startDateParam: '1970-01-01',
      endDateParam: '2100-01-01',
      sortParam: 'brand',
      sortType: 'ASC'
    };
    TrailerDAO.query($scope.filter4).then(function (data) {
        $scope.trailers = data.total;
      });

    $scope.filter5 = {
      searchParam: '',
      first: 0,
      last: 0,
      startDateParam: '1970-01-01',
      endDateParam: '2100-01-01',
      sortParam: 'orderDate',
      sortType: 'ASC'
    };
    OrderDAO.query($scope.filter5).then(function (data) {
      $scope.orders = data.total;
    });

    var startDate = new Date(Date.now());
    var endDate = new Date(Date.now());
    endDate.setMonth(endDate.getMonth() + 1);

    // EXPIRY DATE START-----------------------------------------------------------------------------------------------

    $scope.filter6 = {
      startDateParam: $filter('date')(startDate, 'yyyy-MM-dd'),
      endDateParam: $filter('date')(endDate, 'yyyy-MM-dd'),
      first: 0,
      last: 5,
      sortParam: 'truckInspectionExpiryDate',
      sortType: 'ASC'
    };

    $scope.pagination = {currentPage: 1, itemsPerPage: 5};

    var refreshTruckServices = function () {
      TruckWorkshopDAO.query($scope.filter6).then(function (data) {
        $scope.truckServices = data.result;
        $scope.pagination.totalItems = data.total;
        $scope.trucksCount = data.total;
        $scope.isSomething = $scope.trucksCount > 0;
      });
    };

    $scope.pageChange = function () {
      $scope.filter6.first = ($scope.pagination.currentPage * $scope.pagination.itemsPerPage) - $scope.pagination.itemsPerPage;
      refreshTruckServices();
    };

    $scope.search = function () {
      $scope.pagination.itemsPerPage = $scope.filter6.last;
      refreshTruckServices();
    };

    $scope.filter7 = {
      startDateParam: $filter('date')(startDate, 'yyyy-MM-dd'),
      endDateParam: $filter('date')(endDate, 'yyyy-MM-dd'),
      first: 0,
      last: 5,
      sortParam: 'trailerInspectionExpiryDate',
      sortType: 'ASC'
    };

    $scope.pagination2 = {currentPage: 1, itemsPerPage: 5};

    var refreshTrailerServices = function () {
      TrailerWorkshopDAO.query($scope.filter7).then(function (data) {
        $scope.trailerServices = data.result;
        $scope.pagination2.totalItems = data.total;
        $scope.trailersCount = data.total;
        $scope.isSomething2 = $scope.trailersCount > 0;
      });
    };

    $scope.pageChange2 = function () {
      $scope.filter7.first = ($scope.pagination2.currentPage * $scope.pagination2.itemsPerPage) - $scope.pagination2.itemsPerPage;
      refreshTrailerServices();
    };

    $scope.search2 = function () {
      $scope.pagination2.itemsPerPage = $scope.filter7.last;
      refreshTrailerServices();
    };

    $scope.filter11 = {
      first: 0,
      last: 5,
      startDateParam: $filter('date')(startDate, 'yyyy-MM-dd'),
      endDateParam: $filter('date')(endDate, 'yyyy-MM-dd'),
      sortParam: 'medicalExaminationExpiryDate',
      sortType: 'ASC'
    };

    $scope.pagination3 = {currentPage: 1, itemsPerPage: 5};

    var refreshDriverExpiry = function () {
      DriverDAO.getExpiryDate($scope.filter11).then(function (data) {
        $scope.driverExpiry = data.result;
        $scope.pagination3.totalItems = data.total;
        $scope.driverCount = data.total;
        $scope.isSomething3 = $scope.driverCount > 0;
      });
    };

    $scope.pageChange3 = function () {
      $scope.filter11.first = ($scope.pagination3.currentPage * $scope.pagination3.itemsPerPage) - $scope.pagination3.itemsPerPage;
      refreshDriverExpiry();
    };

    $scope.search3 = function () {
      $scope.pagination3.itemsPerPage = $scope.filter11.last;
      refreshDriverExpiry();
    };

    // EXPIRY DATE END-------------------------------------------------------------------------------------------------

    $scope.filter8 = {
      first: 0,
      last: 0,
      sortParam: 'orderDate',
      sortType: 'DESC'
    };
    OrderDAO.getOngoingOrder($scope.filter8).then(function (data) {
      $scope.ongoing = data.total;
    });

    OrderDAO.getCompletedOrder($scope.filter8).then(function (data) {
      $scope.completed = data.total;
    });

    OrderDAO.getPaidOrder($scope.filter8).then(function (data) {
      $scope.paid = data.total;
    });

    OrderDAO.getUnpaidOrder($scope.filter8).then(function (data) {
      $scope.unpaid = data.total;
    });

    $scope.filter9 = {
      startDateParam: '1900-01-01',
      endDateParam: '2100-01-01',
      first: 0,
      last: 0,
      sortParam: 'truckInspectionDate',
      sortType: 'DESC'
    };
    TruckWorkshopDAO.query($scope.filter9).then(function (data) {
      $scope.trucksServices = data.total;
    });

    $scope.filter10 = {
      startDateParam: '1900-01-01',
      endDateParam: '2100-01-01',
      first: 0,
      last: 0,
      sortParam: 'trailerInspectionDate',
      sortType: 'DESC'
    };
    TrailerWorkshopDAO.query($scope.filter10).then(function (data) {
      $scope.trailersServices = data.total;
    });

    refreshTruckServices();
    refreshTrailerServices();
    refreshDriverExpiry();
  });
