(function () {

  'use strict';

  function TrailerDAO($resource) {
    var api = $resource('http://localhost:8080/api/trailers/:a/:b/:c', null, {
      query: {method: 'GET', headers:{'dateFilter':'true'}},
      save: {method: 'POST'},
      get: {method: 'GET'},
      update: {method: 'PUT'},
      remove: {method: 'DELETE'},
      addTruck: {method: 'POST'},
      getTrucks: {method: 'GET', isArray: true},
      removeTruck: {method: 'DELETE'},
      addTrailerWorkshop: {method: 'POST'},
      getTrailerWorkshops: {method: 'GET', headers: {'dateFilter': 'true'}},
      getOrders: {method: 'GET', headers: {'dateFilter': 'true'}}
    });

    return {
      query: function (filter) {
        return api.query(filter).$promise;
      },
      save: function (data) {
        return api.save(data).$promise;
      },
      get: function (id) {
        return api.get({a: id}).$promise;
      },
      update: function (id, data) {
        return api.update({a: id}, data).$promise;
      },
      remove: function (id) {
        return api.remove({a: id}).$promise;
      },
      addTruck: function (id, truckId) {
        return api.addTruck({a: id, b: 'trucks', c: truckId}, null).$promise;
      },
      getTrucks: function (id) {
        return api.getTrucks({a: id, b: 'trucks'}).$promise;
      },
      removeTruck: function (id, truckId) {
        return api.removeTruck({a: id, b: 'trucks', c: truckId}).$promise;
      },
      addTrailerWorkshop: function (id, data) {
        return api.addTrailerWorkshop({a: id, b: 'trailerWorkshops'}, data).$promise;
      },
      getTrailerWorkshops: function (id, filter) {
        return api.getTrailerWorkshops({
          a: id,
          b: 'trailerWorkshops',
          first: filter.first,
          last: filter.last,
          startDateParam: filter.startDateParam,
          endDateParam: filter.endDateParam,
          sortParam: filter.sortParam,
          sortType: filter.sortType
        }).$promise;
      },
      getOrders: function (id, filter2) {
        return api.getOrders({
          a: id,
          b: 'orders',
          searchParam: filter2.searchParam,
          first: filter2.first,
          last: filter2.last,
          startDateParam: filter2.startDateParam,
          endDateParam: filter2.endDateParam,
          sortParam: filter2.sortParam,
          sortType: filter2.sortType
        }).$promise
      }
    }
  }

  angular.module('transpFrontApp').factory('TrailerDAO', ['$resource', TrailerDAO]);
})();
