(function () {

  'use strict';

  function TrailerWorkshopDAO($resource) {
    var api = $resource('http://localhost:8080/api/trailerWorkshops/:a', null, {
      query: {method: 'GET', headers: {'dateFilter': 'true'}},
      save: {method: 'POST'},
      get: {method: 'GET'},
      update: {method: 'PUT'},
      remove: {method: 'DELETE'}
    });

    return {
      query: function (filter) {
        return api.query(filter).$promise;
      },
      save: function (data) {
        return api.save(data).$promise;
      },
      get: function (id) {
        return api.get({a: id}).$promise;
      },
      update: function (id, data) {
        return api.update({a: id}, data).$promise;
      },
      remove: function (id) {
        return api.remove({a: id}).$promise;
      }
    }
  }

  angular.module('transpFrontApp').factory('TrailerWorkshopDAO', ['$resource', TrailerWorkshopDAO]);
})();
