(function () {

  'use strict';

  function DriverDAO($resource) {
    var api = $resource('http://localhost:8080/api/drivers/:a', null, {
      query: {method: 'GET', headers:{'dateFilter':'true'}},
      save: {method: 'POST'},
      get: {method: 'GET'},
      update: {method: 'PUT'},
      remove: {method: 'DELETE'},
      getExpiryDate: {method: 'GET'}
    });

    return {
      query: function (filter) {
        return api.query(filter).$promise;
      },
      save: function (data) {
        return api.save(data).$promise;
      },
      get: function (id) {
        return api.get({a: id}).$promise;
      },
      update: function (id, data) {
        return api.update({a: id}, data).$promise;
      },
      remove: function (id) {
        return api.remove({a: id}).$promise;
      },
      getExpiryDate: function (filter) {
        return api.getExpiryDate(filter).$promise;
      }
    }
  }

  angular.module('transpFrontApp').factory('DriverDAO', ['$resource', DriverDAO]);
})();

