(function () {

  'use strict';

  function TruckDAO($resource) {
    var api = $resource('http://localhost:8080/api/trucks/:a/:b/:c', null, {
      query: {method: 'GET', headers:{'dateFilter':'true'}},
      save: {method: 'POST'},
      get: {method: 'GET'},
      update: {method: 'PUT'},
      remove: {method: 'DELETE'},
      addDriver: {method: 'POST'},
      getDrivers: {method: 'GET', isArray: true},
      removeDriver: {method: 'DELETE'},
      addTruckWorkshop: {method: 'POST'},
      getTruckWorkshops: {method: 'GET', headers: {'dateFilter': 'true'}}
    });

    return {
      query: function (filter) {
        return api.query(filter).$promise;
      },
      save: function (data) {
        return api.save(data).$promise;
      },
      get: function (id) {
        return api.get({a: id}).$promise;
      },
      update: function (id, data) {
        return api.update({a: id}, data).$promise;
      },
      remove: function (id) {
        return api.remove({a: id}).$promise;
      },
      addDriver: function (id, driverId) {
        return api.addDriver({a: id, b: 'drivers', c: driverId}, null).$promise;
      },
      getDrivers: function (id) {
        return api.getDrivers({a: id, b: 'drivers'}).$promise;
      },
      removeDriver: function (id, driverId) {
        return api.removeDriver({a: id, b: 'drivers', c: driverId}).$promise;
      },
      addTruckWorkshop: function (id, data) {
        return api.addTruckWorkshop({a: id, b: 'truckWorkshops'}, data).$promise;
      },
      getTruckWorkshops: function (id, filter) {
        return api.getTruckWorkshops({
          a: id,
          b: 'truckWorkshops',
          first: filter.first,
          last: filter.last,
          startDateParam: filter.startDateParam,
          endDateParam: filter.endDateParam,
          sortParam: filter.sortParam,
          sortType: filter.sortType
        }).$promise;
      }
    }
  }

  angular.module('transpFrontApp').factory('TruckDAO', ['$resource', TruckDAO]);
})();
