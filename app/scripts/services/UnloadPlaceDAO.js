(function () {

  'use strict';

  function UnloadPlaceDAO($resource) {
    var api = $resource('http://localhost:8080/api/unloadPlaces/:a', null, {
      save: {method: 'POST'},
      get: {method: 'GET'},
      update: {method: 'PUT'},
      remove: {method: 'DELETE'}
    });

    return {
      get: function (id) {
        return api.get({a: id}).$promise;
      },
      update: function (id, data) {
        return api.update({a: id}, data).$promise;
      },
      remove: function (id) {
        return api.remove({a: id}).$promise;
      }
    }

  }

  angular.module('transpFrontApp').factory('UnloadPlaceDAO', ['$resource', UnloadPlaceDAO]);
})();

