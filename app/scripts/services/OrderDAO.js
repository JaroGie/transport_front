(function () {

  'use strict';

  function OrderDAO($resource) {
    var api = $resource('http://localhost:8080/api/orders/:a/:b/:c', null, {
      query: {method: 'GET', headers:{'dateFilter':'true'}},
      save: {method: 'POST'},
      get: {method: 'GET'},
      update: {method: 'PUT'},
      remove: {method: 'DELETE'},
      addTrailer: {method: 'POST'},
      getTrailers: {method: 'GET', isArray: true},
      removeTrailer: {method: 'DELETE'},
      addLoadPlace: {method: 'POST'},
      getLoadPlaces: {method: 'GET', isArray: true},
      addUnloadPlace: {method: 'POST'},
      getUnloadPlaces: {method: 'GET', isArray: true},
      getOngoingOrder: {method: 'GET'},
      getCompletedOrder: {method: 'GET'},
      getUnpaidOrder: {method: 'GET'},
      getPaidOrder: {method: 'GET'}
    });

    return {
      query: function (filter) {
        return api.query(filter).$promise;
      },
      save: function (data) {
        return api.save(data).$promise;
      },
      get: function (id) {
        return api.get({a: id}).$promise;
      },
      update: function (id, data) {
        return api.update({a: id}, data).$promise;
      },
      remove: function (id) {
        return api.remove({a: id}).$promise;
      },
      addTrailer: function (id, trailerId) {
        return api.addTrailer({a: id, b: 'trailers', c: trailerId}, null).$promise;
      },
      getTrailers: function (id) {
        return api.getTrailers({a: id, b: 'trailers'}).$promise;
      },
      removeTrailer: function (id, trailerId) {
        return api.removeTrailer({a: id, b: 'trailers', c: trailerId}).$promise;
      },
      addLoadPlace: function (id, data) {
        return api.addLoadPlace({a: id, b: 'loadPlaces'}, data).$promise;
      },
      getLoadPlaces: function (id) {
        return api.getLoadPlaces({a: id, b: 'loadPlaces'}).$promise;
      },
      addUnloadPlace: function (id, data) {
        return api.addUnloadPlace({a: id, b: 'unloadPlaces'}, data).$promise;
      },
      getUnloadPlaces: function (id) {
        return api.getUnloadPlaces({a: id, b: 'unloadPlaces'}).$promise;
      },
      getOngoingOrder: function (filter) {
        return api.getOngoingOrder({
          a: 'ongoing',
          first: filter.first,
          last: filter.last,
          sortParam: filter.sortParam,
          sortType: filter.sortType
        }).$promise;
      },
      getCompletedOrder: function (filter) {
        return api.getCompletedOrder({
          a: 'completed',
          first: filter.first,
          last: filter.last,
          sortParam: filter.sortParam,
          sortType: filter.sortType
        }).$promise;
      },
      getPaidOrder: function (filter) {
        return api.getPaidOrder({
          a: 'paid',
          first: filter.first,
          last: filter.last,
          sortParam: filter.sortParam,
          sortType: filter.sortType
        }).$promise;
      },
      getUnpaidOrder: function (filter) {
        return api.getUnpaidOrder({
          a: 'unpaid',
          first: filter.first,
          last: filter.last,
          sortParam: filter.sortParam,
          sortType: filter.sortType
        }).$promise;
      }
    }
  }

  angular.module('transpFrontApp').factory('OrderDAO', ['$resource', OrderDAO]);
})();


