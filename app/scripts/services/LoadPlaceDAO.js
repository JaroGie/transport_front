(function () {

  'use strict';

  function LoadPlaceDAO($resource) {
    var api = $resource('http://localhost:8080/api/loadPlaces/:a', null, {
      save: {method: 'POST'},
      get: {method: 'GET'},
      update: {method: 'PUT'},
      remove: {method: 'DELETE'}
    });

    return {
      get: function (id) {
        return api.get({a: id}).$promise;
      },
      update: function (id, data) {
        return api.update({a: id}, data).$promise;
      },
      remove: function (id) {
        return api.remove({a: id}).$promise;
      }
    }

  }

  angular.module('transpFrontApp').factory('LoadPlaceDAO', ['$resource', LoadPlaceDAO]);
})();
