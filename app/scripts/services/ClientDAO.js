(function () {

  'use strict';

  function ClientDAO($resource) {
    var api = $resource('http://localhost:8080/api/clients/:a/:b', null, {
      query: {method: 'GET'},
      save: {method: 'POST'},
      get: {method: 'GET'},
      update: {method: 'PUT'},
      remove: {method: 'DELETE'},
      addOrder: {method: 'POST'},
      getOrders: {method: 'GET', headers: {'dateFilter': 'true'}}
    });

    return {
      query: function (filter) {
        return api.query(filter).$promise;
      },
      save: function (data) {
        return api.save(data).$promise;
      },
      get: function (id) {
        return api.get({a: id}).$promise;
      },
      update: function (id, data) {
        return api.update({a: id}, data).$promise;
      },
      remove: function (id) {
        return api.remove({a: id}).$promise;
      },
      addOrder: function (id, data) {
        return api.addOrder({a: id, b: 'orders'}, data).$promise;
      },
      getOrders: function (id, filter) {
        return api.getOrders({
          a: id,
          b: 'orders',
          searchParam: filter.searchParam,
          first: filter.first,
          last: filter.last,
          startDateParam: filter.startDateParam,
          endDateParam: filter.endDateParam,
          sortParam: filter.sortParam,
          sortType: filter.sortType
        }).$promise;
      }
    }
  }

  angular.module('transpFrontApp').factory('ClientDAO', ['$resource', ClientDAO]);
})();
