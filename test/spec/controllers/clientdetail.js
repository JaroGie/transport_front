'use strict';

describe('Controller: ClientdetailCtrl', function () {

  // load the controller's module
  beforeEach(module('transpFrontApp'));

  var ClientdetailCtrl,
    scope;

  // Initialize the controller and a mock scope
  beforeEach(inject(function ($controller, $rootScope) {
    scope = $rootScope.$new();
    ClientdetailCtrl = $controller('ClientdetailCtrl', {
      $scope: scope
    });
  }));

  it('should attach a list of awesomeThings to the scope', function () {
    expect(scope.awesomeThings.length).toBe(3);
  });
});
