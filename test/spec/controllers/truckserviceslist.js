'use strict';

describe('Controller: TruckserviceslistCtrl', function () {

  // load the controller's module
  beforeEach(module('transpFrontApp'));

  var TruckserviceslistCtrl,
    scope;

  // Initialize the controller and a mock scope
  beforeEach(inject(function ($controller, $rootScope) {
    scope = $rootScope.$new();
    TruckserviceslistCtrl = $controller('TruckserviceslistCtrl', {
      $scope: scope
    });
  }));

  it('should attach a list of awesomeThings to the scope', function () {
    expect(scope.awesomeThings.length).toBe(3);
  });
});
