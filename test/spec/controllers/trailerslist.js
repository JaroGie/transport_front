'use strict';

describe('Controller: TrailerslistCtrl', function () {

  // load the controller's module
  beforeEach(module('transpFrontApp'));

  var TrailerslistCtrl,
    scope;

  // Initialize the controller and a mock scope
  beforeEach(inject(function ($controller, $rootScope) {
    scope = $rootScope.$new();
    TrailerslistCtrl = $controller('TrailerslistCtrl', {
      $scope: scope
    });
  }));

  it('should attach a list of awesomeThings to the scope', function () {
    expect(scope.awesomeThings.length).toBe(3);
  });
});
