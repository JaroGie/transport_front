'use strict';

describe('Controller: OrderdetailCtrl', function () {

  // load the controller's module
  beforeEach(module('transpFrontApp'));

  var OrderdetailCtrl,
    scope;

  // Initialize the controller and a mock scope
  beforeEach(inject(function ($controller, $rootScope) {
    scope = $rootScope.$new();
    OrderdetailCtrl = $controller('OrderdetailCtrl', {
      $scope: scope
    });
  }));

  it('should attach a list of awesomeThings to the scope', function () {
    expect(scope.awesomeThings.length).toBe(3);
  });
});
