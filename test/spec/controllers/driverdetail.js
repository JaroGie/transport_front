'use strict';

describe('Controller: DriverdetailCtrl', function () {

  // load the controller's module
  beforeEach(module('transpFrontApp'));

  var DriverdetailCtrl,
    scope;

  // Initialize the controller and a mock scope
  beforeEach(inject(function ($controller, $rootScope) {
    scope = $rootScope.$new();
    DriverdetailCtrl = $controller('DriverdetailCtrl', {
      $scope: scope
    });
  }));

  it('should attach a list of awesomeThings to the scope', function () {
    expect(scope.awesomeThings.length).toBe(3);
  });
});
