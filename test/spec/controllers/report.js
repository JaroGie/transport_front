'use strict';

describe('Controller: ReportCtrl', function () {

  // load the controller's module
  beforeEach(module('transpFrontApp'));

  var ReporCtrl,
    scope;

  // Initialize the controller and a mock scope
  beforeEach(inject(function ($controller, $rootScope) {
    scope = $rootScope.$new();
    ReporCtrl = $controller('ReportCtrl', {
      $scope: scope
    });
  }));

  it('should attach a list of awesomeThings to the scope', function () {
    expect(scope.awesomeThings.length).toBe(3);
  });
});
