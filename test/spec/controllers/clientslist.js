'use strict';

describe('Controller: ClientslistCtrl', function () {

  // load the controller's module
  beforeEach(module('transpFrontApp'));

  var ClientslistCtrl,
    scope;

  // Initialize the controller and a mock scope
  beforeEach(inject(function ($controller, $rootScope) {
    scope = $rootScope.$new();
    ClientslistCtrl = $controller('ClientslistCtrl', {
      $scope: scope
    });
  }));

  it('should attach a list of awesomeThings to the scope', function () {
    expect(scope.awesomeThings.length).toBe(3);
  });
});
