'use strict';

describe('Controller: TruckdetailCtrl', function () {

  // load the controller's module
  beforeEach(module('transpFrontApp'));

  var TruckdetailCtrl,
    scope;

  // Initialize the controller and a mock scope
  beforeEach(inject(function ($controller, $rootScope) {
    scope = $rootScope.$new();
    TruckdetailCtrl = $controller('TruckdetailCtrl', {
      $scope: scope
    });
  }));

  it('should attach a list of awesomeThings to the scope', function () {
    expect(scope.awesomeThings.length).toBe(3);
  });
});
