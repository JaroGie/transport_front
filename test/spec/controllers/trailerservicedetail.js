'use strict';

describe('Controller: TrailerservicedetailCtrl', function () {

  // load the controller's module
  beforeEach(module('transpFrontApp'));

  var TrailerservicedetailCtrl,
    scope;

  // Initialize the controller and a mock scope
  beforeEach(inject(function ($controller, $rootScope) {
    scope = $rootScope.$new();
    TrailerservicedetailCtrl = $controller('TrailerservicedetailCtrl', {
      $scope: scope
    });
  }));

  it('should attach a list of awesomeThings to the scope', function () {
    expect(scope.awesomeThings.length).toBe(3);
  });
});
