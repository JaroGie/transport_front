'use strict';

describe('Controller: UnloadplacedetailCtrl', function () {

  // load the controller's module
  beforeEach(module('transpFrontApp'));

  var UnloadplacedetailCtrl,
    scope;

  // Initialize the controller and a mock scope
  beforeEach(inject(function ($controller, $rootScope) {
    scope = $rootScope.$new();
    UnloadplacedetailCtrl = $controller('UnloadplacedetailCtrl', {
      $scope: scope
    });
  }));

  it('should attach a list of awesomeThings to the scope', function () {
    expect(scope.awesomeThings.length).toBe(3);
  });
});
