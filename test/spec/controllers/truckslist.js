'use strict';

describe('Controller: TruckslistCtrl', function () {

  // load the controller's module
  beforeEach(module('transpFrontApp'));

  var TruckslistCtrl,
    scope;

  // Initialize the controller and a mock scope
  beforeEach(inject(function ($controller, $rootScope) {
    scope = $rootScope.$new();
    TruckslistCtrl = $controller('TruckslistCtrl', {
      $scope: scope
    });
  }));

  it('should attach a list of awesomeThings to the scope', function () {
    expect(scope.awesomeThings.length).toBe(3);
  });
});
