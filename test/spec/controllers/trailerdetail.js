'use strict';

describe('Controller: TrailerdetailCtrl', function () {

  // load the controller's module
  beforeEach(module('transpFrontApp'));

  var TrailerdetailCtrl,
    scope;

  // Initialize the controller and a mock scope
  beforeEach(inject(function ($controller, $rootScope) {
    scope = $rootScope.$new();
    TrailerdetailCtrl = $controller('TrailerdetailCtrl', {
      $scope: scope
    });
  }));

  it('should attach a list of awesomeThings to the scope', function () {
    expect(scope.awesomeThings.length).toBe(3);
  });
});
