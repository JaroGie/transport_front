'use strict';

describe('Controller: DriverslistCtrl', function () {

  // load the controller's module
  beforeEach(module('transpFrontApp'));

  var DriverslistCtrl,
    scope;

  // Initialize the controller and a mock scope
  beforeEach(inject(function ($controller, $rootScope) {
    scope = $rootScope.$new();
    DriverslistCtrl = $controller('DriverslistCtrl', {
      $scope: scope
    });
  }));

  it('should attach a list of awesomeThings to the scope', function () {
    expect(scope.awesomeThings.length).toBe(3);
  });
});
