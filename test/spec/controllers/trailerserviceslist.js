'use strict';

describe('Controller: TrailerserviceslistCtrl', function () {

  // load the controller's module
  beforeEach(module('transpFrontApp'));

  var TrailerserviceslistCtrl,
    scope;

  // Initialize the controller and a mock scope
  beforeEach(inject(function ($controller, $rootScope) {
    scope = $rootScope.$new();
    TrailerserviceslistCtrl = $controller('TrailerserviceslistCtrl', {
      $scope: scope
    });
  }));

  it('should attach a list of awesomeThings to the scope', function () {
    expect(scope.awesomeThings.length).toBe(3);
  });
});
