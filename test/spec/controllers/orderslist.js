'use strict';

describe('Controller: OrderslistCtrl', function () {

  // load the controller's module
  beforeEach(module('transpFrontApp'));

  var OrderslistCtrl,
    scope;

  // Initialize the controller and a mock scope
  beforeEach(inject(function ($controller, $rootScope) {
    scope = $rootScope.$new();
    OrderslistCtrl = $controller('OrderslistCtrl', {
      $scope: scope
    });
  }));

  it('should attach a list of awesomeThings to the scope', function () {
    expect(scope.awesomeThings.length).toBe(3);
  });
});
