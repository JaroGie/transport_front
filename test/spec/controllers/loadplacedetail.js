'use strict';

describe('Controller: LoadplacedetailCtrl', function () {

  // load the controller's module
  beforeEach(module('transpFrontApp'));

  var LoadplacedetailCtrl,
    scope;

  // Initialize the controller and a mock scope
  beforeEach(inject(function ($controller, $rootScope) {
    scope = $rootScope.$new();
    LoadplacedetailCtrl = $controller('LoadplacedetailCtrl', {
      $scope: scope
    });
  }));

  it('should attach a list of awesomeThings to the scope', function () {
    expect(scope.awesomeThings.length).toBe(3);
  });
});
